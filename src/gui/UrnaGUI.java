/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import candidatos.Candidato;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import urnacliente.Urna;

/**
 *
 * @author kollins
 */
public class UrnaGUI {

    private Urna urna;
    private JFrame janela = new JFrame();
    private JPanel pane1 = new JPanel();
    private JPanel pane2 = new JPanel();
    private JPanel titulo = new JPanel();
    private JPanel teclado = new JPanel();
    private JPanel display = new JPanel();
    private JLabel jus;
    private JTextPane id = new JTextPane();
    private JButton[] botao = new JButton[10];
    private JButton btconfirma = new JButton();
    private JButton btcorrige = new JButton();
    private JButton btbranco = new JButton();
    private String codigo = "";
    GridBagConstraints c = new GridBagConstraints();
    HashMap<Integer, Candidato> listaCandidatos;
    ArrayList<JPanel> candidato = new ArrayList<JPanel> ();
    ArrayList<JTextPane> form = new ArrayList<JTextPane> ();
    ArrayList<JLabel> candImg = new ArrayList<JLabel> ();

    public UrnaGUI(Urna urna) {
        this.urna = urna;
    }
    
    //Imprime lista de candidatos
    public void print(HashMap<Integer, Candidato> listaCandidatos) {
 
        pane2.removeAll();
        int c = 0;
        if (!listaCandidatos.isEmpty()) {

            pane2.setLayout(new BoxLayout(pane2, 1));

            for (Candidato cand : listaCandidatos.values()) {
                candidato.add(new JPanel());
                form.add(new JTextPane());
                candImg.add(new JLabel());
                candidato.get(c).setLayout(new BorderLayout());
                candidato.get(c).setPreferredSize(new Dimension(600, 400));
                candImg.get(c).setIcon(cand.getImg_candidato());
                candImg.get(c).setPreferredSize(new Dimension(300, 400));
                form.get(c).setEditable(false);
                form.get(c).setPreferredSize(new Dimension(350, 400));
                form.get(c).setBackground(Color.LIGHT_GRAY);
                form.get(c).setFont(new Font("ARIAL", Font.BOLD, 14));
                form.get(c).setText("\n\n\n\n\n CANDIDATO" + "\n\n Número: " + cand.getCodigo_votacao() + "\n\n Nome: " + cand.getNome_candidato() + "\n\n Partido: " + cand.getPartido());
                candidato.get(c).add(candImg.get(c), BorderLayout.EAST);
                candidato.get(c).add(form.get(c), BorderLayout.WEST);
                candidato.get(c).updateUI();
                pane2.add(candidato.get(c));
                c = c + 1;
            }
            
        } else {

            JLabel nulo = new JLabel("Voto Nulo");
            nulo.setFont(new Font("Impact", Font.BOLD, 40));
            pane2.setLayout(new GridBagLayout());
            pane2.add(nulo);
        }
        
        pane2.updateUI();

    }
    
    //Voto Branco
    public void printBranco() {

        pane2.removeAll();
        JLabel branco = new JLabel("Voto Branco");
        branco.setFont(new Font("Impact", Font.BOLD, 40));
        pane2.setLayout(new GridBagLayout());
        pane2.add(branco);
        pane2.updateUI();

    }
    
    //Voto Confirmado
    public void printConfirma() {

        pane2.removeAll();
        JLabel branco = new JLabel("VOTO CONFIRMADO");
        branco.setFont(new Font("Impact", Font.BOLD, 40));
        pane2.setLayout(new GridBagLayout());
        pane2.add(branco);
        pane2.updateUI();
  

    }
    
    //Atualiza display
    public void update(){
      
        id.setText("Voto\n" + this.getCodigo());
        
    }

    public void show() {
        
        Border border = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
        teclado.setLayout(new GridBagLayout());
        teclado.setBackground(java.awt.Color.black);
        
        //Adicionando botoes do teclado
        int n = 1;
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                botao[n] = new JButton();
                botao[n].setText(String.valueOf(n));
                botao[n].setForeground(java.awt.Color.white);
                botao[n].setBackground(java.awt.Color.black);
                botao[n].setBorder((BorderFactory.createMatteBorder(2, 2, 2, 2, java.awt.Color.white)));
                botao[n].setActionCommand(String.valueOf(n));
                botao[n].addActionListener(urna);
                c.gridx = j;
                c.gridy = i;
                c.insets = new Insets(2, 2, 2, 2);
                c.fill = GridBagConstraints.HORIZONTAL;
                teclado.add(botao[n], c);
                n = n + 1;
            }
        }
        
        //Botao zero
        botao[0] = new JButton();
        botao[0].setText(String.valueOf(0));
        botao[0].setForeground(java.awt.Color.white);
        botao[0].setBackground(java.awt.Color.black);
        botao[0].setBorder((BorderFactory.createMatteBorder(2, 2, 2, 2, java.awt.Color.white)));
        botao[0].addActionListener(urna);
        c.gridx = 2;
        c.gridy = 4;
        teclado.add(botao[0], c);

        //Botao Corrige
        btcorrige.setForeground(java.awt.Color.black);
        btcorrige.setText("CORRIGE");
        btcorrige.setActionCommand("Corrige");
        btcorrige.addActionListener(urna);
        btcorrige.setBackground(java.awt.Color.red);
        c.gridx = 2;
        c.gridy = 5;
        teclado.add(btcorrige, c);
        
        //Botao Confirma
        btconfirma.setForeground(java.awt.Color.black);
        btconfirma.setText("CONFIRMA");
        btconfirma.setActionCommand("Confirma");
        btconfirma.addActionListener(urna);
        btconfirma.setBackground(java.awt.Color.green);
        c.gridx = 3;
        c.gridy = 5;
        teclado.add(btconfirma, c);
        
        //Botao Branco
        btbranco.setForeground(java.awt.Color.black);
        btbranco.setText("BRANCO");
        btbranco.setActionCommand("Branco");
        btbranco.addActionListener(urna);
        btbranco.setBackground(java.awt.Color.white);
        c.gridx = 1;
        c.gridy = 5;
        teclado.add(btbranco, c);

        //Adicionando JPanel - Display
        display.setBackground(Color.LIGHT_GRAY);
        id.setFont(new Font("ARIAL", Font.BOLD, 14));
        id.setEditable(false);
        id.setPreferredSize(new Dimension(100,40));
        id.setText("Voto\n" + codigo);
        display.add(id);
        
        //Centralizar campo text
        StyledDocument doc = id.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

        //Adicionando JLabel - Justiça Federal
        ImageIcon jus_logo = new ImageIcon(getClass().getResource("/imagens/jus_logo.jpg"));
        jus = new JLabel("JUSTIÇA ELEITORAL", jus_logo, SwingConstants.CENTER);
        titulo.add(jus);
        titulo.setBackground(java.awt.Color.white);
        titulo.setBorder((BorderFactory.createMatteBorder(10, 10, 1, 10, java.awt.Color.BLACK)));
        display.setBorder((BorderFactory.createMatteBorder(30, 135, 1, 150, java.awt.Color.BLACK)));
        teclado.setBorder((BorderFactory.createMatteBorder(29, 10, 1, 10, java.awt.Color.BLACK)));
        pane1.setLayout(new BoxLayout(pane1, 1));
        pane2.setLayout(new BoxLayout(pane2, 1));
        pane1.add(titulo);
        pane1.add(display);
        pane1.add(teclado);
        pane1.updateUI();
        pane2.updateUI();
        pane2.setSize(700, 400);
        janela.add(pane1, BorderLayout.EAST);
        janela.add(pane2, BorderLayout.WEST);

        //Adicionando Scroll
        JScrollPane scroller = new JScrollPane(pane2);
        janela.getContentPane().add(scroller);
        janela.setSize(1100, 400);
        janela.addWindowListener(urna);
        janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        janela.setVisible(true);
    }

    public String getCodigo() {
        return codigo;
    }

    public void setText(String text) {
        codigo = text;
    }

    public void catText(String text) {
        codigo = codigo.concat(text);
        filter(codigo);
    }

    /**
     * Apaga o conteúdo de uma String pré codificada (Código digitado na urna)
     */
    public void clearText() {
        codigo = "";
    }
    
    //Filtra candidato por número de votação
    private void filter(String codigo) {
        
        String codigoVot;
        boolean contain = true;

        HashMap<Integer, Candidato> listaFiltrada = new HashMap();

        for (Candidato cand : listaCandidatos.values()) {

            codigoVot = String.valueOf(cand.getCodigo_votacao());

            for (int i = 0; i < codigo.length(); i++) {
                if (codigoVot.charAt(i) != codigo.charAt(i)) {
                    contain = false;
                    break;
                }
            }

            if (contain) {
                listaFiltrada.put(cand.getCodigo_votacao(), cand);
            }

            contain = true;
        }
        
        print(listaFiltrada);
    }

    public void setListaCandidatos(HashMap<Integer, Candidato> listaCandidatos) {
        this.listaCandidatos = listaCandidatos;
    }

    //Imprime Estatísticas da votação(apuração)
    public void printStat(HashMap<Integer, Integer> votacao, String vot){
       
        int totalVotos = 0;
        String voto = vot;

        print(listaCandidatos);
        for (int votos : votacao.values()) {
            totalVotos += votos;
            
        }
        for (Candidato candI : listaCandidatos.values()) {
              if(String.valueOf(candI.getCodigo_votacao()).equals(voto)){
                  voto = "Confirmado";
                  break;
              }
              else if(voto.equals("-1")){
                  voto = "Branco";
                  break;
              }
              
        }
        if(!voto.equals("Confirmado") && !voto.equals("Branco")) voto = "Nulo";
        int n = 0;
        for (Candidato cand : listaCandidatos.values()) {
            form.get(n).setText("\n\n\n\n\n\n\n Candidato:"+cand.getNome_candidato()+"\n\n Votos:" + ((float)votacao.get(cand.getCodigo_votacao())/totalVotos)*100 + " %");
            n=n+1;
        }
        
        //Adicionando Estatística Votos Brancos
        JPanel branco = new JPanel();
        branco.setLayout(new BorderLayout());
        branco.setPreferredSize(new Dimension(600, 400));
        JLabel candImg = new JLabel("Brancos");
        candImg.setFont(new Font("Impact", Font.BOLD, 40));
        candImg.setPreferredSize(new Dimension(250, 400));
        JTextPane form = new JTextPane();
        form.setEditable(false);
        form.setPreferredSize(new Dimension(350, 400));
        form.setBackground(Color.LIGHT_GRAY);
        form.setFont(new Font("ARIAL", Font.BOLD, 14));
        form.setText("\n\n\n\n\n\n\n Brancos:"+"\n\n Votos:" + ((float)votacao.get(-1) / totalVotos) * 100 + " %");
        branco.add(candImg, BorderLayout.EAST);
        branco.add(form, BorderLayout.WEST);
        branco.updateUI();
        pane2.add(branco);
        
        //Adicionando Estatística Votos Nulos
        JPanel nulos = new JPanel();
        nulos.setLayout(new BorderLayout());
        nulos.setPreferredSize(new Dimension(600, 400));
        JLabel candImgI = new JLabel("Nulos");
        candImgI.setFont(new Font("Impact", Font.BOLD, 40));
        candImgI.setPreferredSize(new Dimension(250, 400));
        JTextPane formI = new JTextPane();
        formI.setEditable(false);
        formI.setPreferredSize(new Dimension(350, 400));
        formI.setBackground(Color.LIGHT_GRAY);
        formI.setFont(new Font("ARIAL", Font.BOLD, 14));
        formI.setText("\n\n\n\n\n\n\n Nulos:"+"\n\n Votos:" + ((float)votacao.get(0) / totalVotos) * 100 + " %");
        nulos.add(candImgI, BorderLayout.EAST);
        nulos.add(formI, BorderLayout.WEST);
        nulos.updateUI();
        pane2.add(nulos);
        id.setText("Voto\n" + voto);
        
        }
            
    }




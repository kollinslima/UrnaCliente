/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urnacliente;

import candidatos.Candidato;
import gui.UrnaGUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static java.lang.System.exit;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kollins
 */
public class Urna implements ActionListener, WindowListener {

    private final int SERVER_PORT = 40102;
    private final String SERVER = "cosmos.lasdpc.icmc.usp.br";
    private String code = "";
    
    private Socket comunicacao;
    private ObjectOutputStream envia;
    private ObjectInputStream recebe;

    private UrnaGUI gui;
    private HashMap<Integer, Candidato> listaCandidatos = null;
    private HashMap<Integer, Integer> votacao = null;
    private LinkedList<Integer> votos; //Histórico de votos

    public Urna() {
        votos = new LinkedList();
        try {
            comunicacao = new Socket(InetAddress.getByName(SERVER), SERVER_PORT);
            envia = new ObjectOutputStream(comunicacao.getOutputStream());
            recebe = new ObjectInputStream(comunicacao.getInputStream());
            gui = new UrnaGUI(this);
        } catch (IOException ex) {
            System.out.println("Erro de conexao com o servidor");
            exit(1);
        }

    }

    void start() {
        gui.show();
        
    }
    
    private void pedeCandidatos(){
        try {
            envia.writeObject("candidatos");
            recebeCandidatos();
        } catch (IOException ex) {
            Logger.getLogger(Urna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void recebeCandidatos() {
        try {
            listaCandidatos = (HashMap<Integer, Candidato>) recebe.readObject();
            gui.setListaCandidatos(listaCandidatos);
            gui.print(listaCandidatos);
        } catch (IOException|ClassNotFoundException ex) {
            Logger.getLogger(Urna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        String command = e.getActionCommand();

        switch (command) {
          
            case "Confirma":
   
                if(!code.equals("-1")){
                    code = gui.getCodigo();
                }
                enviaVotos(code);
                recebeAtualizacoes();
                gui.clearText();
                gui.printStat(votacao, code);
                code = "";
                break;
                
            case "Corrige":
                gui.clearText();
                gui.print(listaCandidatos);
                gui.update();
                break;
                
            case "Branco":
                gui.printBranco();
                code = "-1";
                break;
                
            case "Nulo":
                break;
                
            default:
                gui.catText(command);
                gui.update();
                break;
        }
    }

    void enviaVotos(String codigo) {
        try {
            if(code.equals("-1")) {
                votos.add(-1);
            }
            else{
                votos.add(Integer.parseInt(codigo));
            }
            String msg = "voto:"+codigo+":"+votos.size();
            System.out.println(msg);
            envia.writeObject(msg);
        } catch (IOException ex) {
            //Apresentar erro na tela e pedir para enviar novamente.
        }
    }

    void recebeAtualizacoes() {
        try {
            votacao = (HashMap<Integer, Integer>) recebe.readUnshared();
            if(votacao == null){
                System.out.println("erro - recontagem");
                enviaHistorico();
            }
            System.out.println("votos - " + votacao);
            //gui.print(listaCandidatos);
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Recebe Votos - Erro de Comunicação");
        }
    }
    
    private void enviaHistorico() throws IOException{
        envia.writeObject(votos);
    }
    
    private void encerrarSistema() {
        try {
            envia.writeObject("fechar");
            envia.close();
            recebe.close();
            System.out.println("Sistema encerrado");
        } catch (IOException ex) {
            Logger.getLogger(Urna.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {//envia histórico e encerra a execução
        encerrarSistema();
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }
}

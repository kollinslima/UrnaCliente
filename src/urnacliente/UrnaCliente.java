package urnacliente;

import java.io.IOException;

public class UrnaCliente {

    public static void main(String[] args) throws IOException {

        Urna urna = new Urna();

        urna.start();
        urna.recebeCandidatos();

    }

}
